# Maintainer: Benjamin Klettbach <b dot klettbach at gmail dot com >
# Contributor: Jonathan Steel <jsteel at archlinux.org>
# Contributor: ArcticVanguard <LideEmily at gmail dot com>
# Contributor: ledti <antergist at gmail dot com>

pkgname=obs-studio-git
pkgver=21.1.2
pkgrel=1
pkgdesc="Free and open source software for video recording and live streaming."
arch=("i686" "x86_64")
url="https://github.com/obsproject/obs-studio"
license=("GPL2")
depends=("ffmpeg" "jansson" "libxinerama" "libxkbcommon-x11"
         "qt5-x11extras" "curl" "gtk-update-icon-cache")
makedepends=("cmake" "git" "libfdk-aac" "libxcomposite" "x264" "jack"
             "vlc" "swig" "luajit" "python")
optdepends=("libfdk-aac: FDK AAC codec support"
            "libxcomposite: XComposite capture support"
            "jack: JACK Support"
            "vlc: VLC Media Source"
            "swig: Scripting"
            "luajit: Lua scripting"
            "python: Python scripting")
provides=("obs-studio=$pkgver")
conflicts=("obs-studio")
source=("$pkgname::git+https://github.com/obsproject/obs-studio.git#branch=master"
       "git+https://github.com/Mixer/ftl-sdk.git"
       ffmpeg-vaapi.patch)
md5sums=("SKIP" "SKIP" "SKIP")

pkgver() {
  cd "${srcdir}"/${pkgname}
  git describe --tags $(git rev-list --tags --max-count=1) | sed 's/-/_/g'
}

prepare() {
  cd "${srcdir}"/${pkgname}
  latesttag=$(git describe --tags $(git rev-list --tags --max-count=1))
  
  # this if/else is a temp check for a compile error with 21.1.2 on linux. 
  if [[ "$latesttag" == "21.1.2" ]]; then
      git checkout f81d106b2a4f196ae805eeefa29248248b153483
  else
      git checkout $latesttag
  fi
  patch -Np1 < "${srcdir}"/ffmpeg-vaapi.patch
  git config submodule.plugins/obs-outputs/ftl-sdk.url "${srcdir}"/ftl-sdk
  git submodule update
}

build() {
  cd "${srcdir}"/$pkgname

  mkdir -p build; cd build

  cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DOBS_VERSION_OVERRIDE=$pkgver ..

  make
}

package() {
  cd "${srcdir}"/$pkgname/build

  make install DESTDIR="${pkgdir}"
}

# vim: ts=2:sw=2:expandtab

